# group explorer

A script to explore Group and Group Member information from the GitLab GraphQL API.

More information on the GitLab's GraphQL API can be found at: https://docs.gitlab.com/ee/api/graphql/index.html

## requirements

- ruby 2.7+; ideally ruby 3.1+
- bundler
- GitLab API key with sufficient privileges to access group and group member data

## usage

- `cp conifig.toml.example config.toml`
- update `config.toml` with group paths (see: config.toml.example)
- `bundle install`
- `GITLAB_PERSONAL_TOKEN=SETEC-ASTRONOMY bundle exec ruby group_members.rb`
