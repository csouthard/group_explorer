require "bundler/setup"
require "dotenv/load"
require "httparty"
require "perfect_toml"

class GroupMembers
  include HTTParty

  Dotenv.require_keys("GITLAB_PERSONAL_TOKEN")

  PRIVATE_TOKEN = ENV.fetch("GITLAB_PERSONAL_TOKEN")
  HEADERS = {"Private-Token": PRIVATE_TOKEN}
  BASE_URL = "https://gitlab.com/api/v4"

  def initialize()
  end

  def fetch_group(group_fullpath)
    results = {}
    variables = {}
    query_args = "query"

    # remove or replace Group Member relation argument
    # DIRECT: Members in the group itself
    # INHERITED: Members in the group's ancestor groups
    # DESCENDANTS: Members in the group's subgroups
    # SHARED_FROM_GROUPS Invited group's members.
    #
    relations = "DIRECT"
    group_members_args = "groupMembers (relations: #{relations})"

    # NOTE(chaserx): you can replace `group()` with `groups` if you have:
    # a limited number of groups maybe less than 20 to avoid additional pagination queries
    # and if the owner of the personal key has sufficient privileges to all groups.
    loop do
      group_member_query = <<-HEREDOC
        #{query_args} {
          group(fullPath: "#{group_fullpath}") {
            id
            billableMembersCount
            #{group_members_args} {
              edges{
                node{
                  id
                  user {
                    name
                    publicEmail
                    webUrl
                    bot
                    state
                  }
                  accessLevel{
                    stringValue
                  }
                }
              }
              pageInfo {
                endCursor
                startCursor
                hasNextPage
              }
            }
          }
        }
      HEREDOC

      response = HTTParty.post(
        "https://gitlab.com/api/graphql",
        headers: {
          Authorization: "Bearer #{PRIVATE_TOKEN}",
          "Content-Type": "application/json"
        },
        body: {"query" => group_member_query, "variables" => variables}.to_json
      )

      if response.parsed_response.key?("errors")
        puts "GitLab returned an error: #{response.parsed_response["errors"]}"
        raise StopIteration
      end

      results.merge!(response.parsed_response.to_hash)

      if response.parsed_response.dig("data", "group", "groupMembers", "pageInfo", "hasNextPage")
        end_cursor = response.parsed_response.dig("data", "group", "groupMembers", "pageInfo", "endCursor")
        variables = {"after" => end_cursor}
        query_args = "query($after:String)"
        group_members_args = "groupMembers(after:$after, relations: #{relations})"
      else
        break
      end
    end
    results
  end
end

config = PerfectTOML.load_file("config.toml", symbolize_names: true)
GROUPS = config[:groups].freeze
GroupMember = Struct.new(:name, :publicEmail, :webUrl, :access_level, :bot, :state) do
  def to_s
    "#{name}, #{publicEmail}, #{webUrl}, #{access_level}, bot: #{bot}, state: #{state}"
  end
end

GROUPS.each do |group|
  group_data = GroupMembers.new.fetch_group(group)
  puts "billable members: #{group_data.dig("data", "group", "billableMembersCount")}"
  group_members = group_data.dig("data", "group", "groupMembers", "edges")&.map{|edge| GroupMember.new(edge["node"]["user"]["name"], edge["node"]["user"]["publicEmail"], edge["node"]["user"]["webUrl"], edge["node"]["accessLevel"]["stringValue"], edge["node"]["user"]["bot"], edge["node"]["user"]["state"])}
  if group_members.nil?
    puts "Could not find group members for #{group}"
  else
    puts group_members
  end
end
